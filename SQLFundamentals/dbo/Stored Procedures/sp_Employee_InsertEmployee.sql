﻿CREATE PROCEDURE [dbo].[sp_Employee_InsertEmployee]
	@EmployeeName NVARCHAR(100) = NULL,
	@FirstName NVARCHAR(50) = NULL,
	@LastName NVARCHAR(50) = NULL,
	@CompanyName NVARCHAR(20),
	@Street NVARCHAR(50),
	@Position NVARCHAR(30) = NULL,
	@City NVARCHAR(20) = NULL,
	@State NVARCHAR(50) = NULL,
	@ZipCode NVARCHAR(50) = NULL
	AS
	IF ((NULLIF(TRIM(@FirstName), '') IS NOT NULL) OR (NULLIF(TRIM(@LastName), '') IS NOT NULL) OR (NULLIF(TRIM(@EmployeeName), '') IS NOT NULL))
	BEGIN
		DECLARE @AddressId AS INT;
		INSERT INTO [dbo].[Address] (Street, City, State, ZipCode) VALUES (@Street, @City, @State, @ZipCode);
		SET @AddressId = @@IDENTITY;
		DECLARE @PersonId AS INT;
		INSERT INTO [dbo].[Person] (FirstName, LastName) VALUES (ISNULL(@FirstName, ''), ISNULL(@LastName, ''));
		SET @PersonId = @@IDENTITY;
		INSERT INTO [dbo].[Employee] (AddressId, PersonId, CompanyName, Position, EmployeeName) VALUES (@AddressId, @PersonId, SUBSTRING(@CompanyName, 1, 20), @Position, @EmployeeName);
	END
