﻿CREATE VIEW [dbo].[EmployeeInfo]
	AS
	SELECT [Employee].[Id] as EmployeeId,  
	COALESCE([Employee].[EmployeeName], CONCAT([Person].[FirstName],' ',[Person].[LastName])) as EmployeeFullName,
	CONCAT([Addres].[ZipCode],'_',[Addres].[State],', ',[Addres].[City],'-',[Addres].[Street]) as EmployeeFullAddress,
	CONCAT([Employee].[CompanyName],'(',[Employee].[Position],')') as EmployeeCompanyInfo
	FROM [dbo].[Employee] AS Employee
	LEFT JOIN [dbo].[Person] AS Person ON [Employee].[PersonId] = [Person].[Id]
	LEFT JOIN [dbo].[Address] AS Addres ON [Employee].[AddressId] = [Addres].[Id]
	ORDER BY [Employee].[CompanyName], [Employee].[Position] OFFSET 0 ROWS

