﻿CREATE TRIGGER [tr_Employee_Insert]
	ON [dbo].[Employee]
	AFTER INSERT
	AS
	BEGIN
		INSERT INTO [dbo].[Company] (Name, AddressId) SELECT CompanyName, AddressId FROM INSERTED;
	END