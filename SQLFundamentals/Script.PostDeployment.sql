﻿IF NOT EXISTS(SELECT * FROM [dbo].[Person])  
BEGIN  
    SET IDENTITY_INSERT [dbo].[Person] ON;
    INSERT INTO [dbo].[Person] ([Id],[FirstName],[LastName]) VALUES (1,'Ann','Swon');
    INSERT INTO [dbo].[Person] ([Id],[FirstName],[LastName]) VALUES (2,'Jack','Tour');
    INSERT INTO [dbo].[Person] ([Id],[FirstName],[LastName]) VALUES (3,'John','Morris');
    INSERT INTO [dbo].[Person] ([Id],[FirstName],[LastName]) VALUES (4,'Jeremy','White');
    INSERT INTO [dbo].[Person] ([Id],[FirstName],[LastName]) VALUES (5,'Walter','Loop');
    SET IDENTITY_INSERT [dbo].[Person] OFF;
END  

IF NOT EXISTS(SELECT * FROM [dbo].[Address]) 
BEGIN  
    SET IDENTITY_INSERT [dbo].[Address] ON;
    INSERT INTO [dbo].[Address] ([Id],[Street],[City],[State],[ZipCode]) VALUES (1,'West street','Edinburgh','AI',12435);
    INSERT INTO [dbo].[Address] ([Id],[Street],[City],[State],[ZipCode]) VALUES (2,'North street','Paris','WA',24567);
    INSERT INTO [dbo].[Address] ([Id],[Street],[City],[State],[ZipCode]) VALUES (3,'South street','London','TI',78987);
    INSERT INTO [dbo].[Address] ([Id],[Street],[City],[State],[ZipCode]) VALUES (4,'Main street','Bristol','WA',24898);
    INSERT INTO [dbo].[Address] ([Id],[Street],[City],[State],[ZipCode]) VALUES (5,'South street','York','TL',97457);
    SET IDENTITY_INSERT [dbo].[Address] OFF;
END  

IF NOT EXISTS(SELECT * FROM [dbo].[Company])
BEGIN  
    SET IDENTITY_INSERT [dbo].[Company] ON;
    INSERT INTO [dbo].[Company] ([Id],[Name],[AddressId]) VALUES (1,'Google',3);
    INSERT INTO [dbo].[Company] ([Id],[Name],[AddressId]) VALUES (2,'Apple',2);
    INSERT INTO [dbo].[Company] ([Id],[Name],[AddressId]) VALUES (3,'Audi',1);
    INSERT INTO [dbo].[Company] ([Id],[Name],[AddressId]) VALUES (4,'Huawei',5);
    INSERT INTO [dbo].[Company] ([Id],[Name],[AddressId]) VALUES (5,'Xiaomi',4);
    SET IDENTITY_INSERT [dbo].[Company] OFF;
END  

IF NOT EXISTS(SELECT * FROM [dbo].[Employee])
BEGIN  
    SET IDENTITY_INSERT [dbo].[Employee] ON;
    INSERT INTO [dbo].[Employee] ([Id],[AddressId],[PersonId],[CompanyName],[Position],[EmployeeName]) VALUES (1,3,3,'Google','Developer','Junior');
    INSERT INTO [dbo].[Employee] ([Id],[AddressId],[PersonId],[CompanyName],[Position],[EmployeeName]) VALUES (2,2,1,'Apple','Designer','Senior');
    INSERT INTO [dbo].[Employee] ([Id],[AddressId],[PersonId],[CompanyName],[Position],[EmployeeName]) VALUES (3,1,2,'Audi','Developer','Middle');
    INSERT INTO [dbo].[Employee] ([Id],[AddressId],[PersonId],[CompanyName],[Position],[EmployeeName]) VALUES (4,5,4,'Huawei','Designer','Senior');
    INSERT INTO [dbo].[Employee] ([Id],[AddressId],[PersonId],[CompanyName],[Position],[EmployeeName]) VALUES (5,4,5,'Xiaomi','Developer','Middle');
    SET IDENTITY_INSERT [dbo].[Employee] OFF;
END